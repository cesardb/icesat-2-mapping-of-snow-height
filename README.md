# ICESat-2 Mapping of Snow Height

Code to retrieve snow height from ICESat-2 ATL06 data (snow-on point) and a snow-off Digital Elevation Model.
Written by Simon Gascoin and César Deschamps-Berger with help from David Shean, Hannah Besso and Ambroise Guiot.
This is a simplified version of the code to illustrate the main steps.
