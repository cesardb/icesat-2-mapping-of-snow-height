#!/usr/bin/python
#-*- coding: utf-8 -*-

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import numpy as np
from osgeo import gdal
import scipy.interpolate
import scipy.optimize as optimization
import os
import argparse
import csv
import sys

np.warnings.filterwarnings('ignore')

def f(x, a, b, c): 
    y = a * np.cos(np.deg2rad(b-x))  + c
    return y

def nmad(Z): 
    return 1.4826*np.nanmedian(abs(Z-np.nanmedian(Z)))


def mymode(data,limit,res):
    data[abs(data)>limit]=np.nan
    nbins=int((2*limit)/res)+1
    hist,bins=np.histogram(data,bins=nbins,range=(-limit,limit))
    nmax=np.where(hist==np.max(hist))[0][0]
    mode=(bins[nmax]+bins[nmax+1])/2
    return mode

def mygetRSTdata(inRST):
    print('\nImporting data from raster: '+inRST)
    ds = gdal.Open(inRST)
    Band = ds.GetRasterBand(1)
    RST = Band.ReadAsArray()
    RST = np.flip( RST, 0)
    RST = RST.astype('float32')
    if Band.GetNoDataValue()==None:
        print('! Warning ! No data value is empty for '+inRST+'. Masking might not work properly.')
        mask = np.ones(np.shape(RST)).astype(bool)
    else:
        mask = ((RST != Band.GetNoDataValue()))
    Band=None
    ds = None
    return RST,mask

def mygetRSTgrid(inRST):  
    print('\nImporting grid from raster: '+inRST)
    #with xr.open_rasterio(inRST) as srcRST:
    #    X = 
    ds = gdal.Open(inRST) 
    x0, dx, dxdy, y0, dydx, dy = ds.GetGeoTransform()
    XSize = ds.RasterXSize
    YSize = ds.RasterYSize
    x1 = x0 + XSize*dx
    y1 = y0 + YSize*dy
    X = np.arange(x0,x1,dx)
    Y = np.arange(y0,y1,dy)
    Y = np.flip(Y,0)
    return X,Y


def mygetNnK_dXdY(dH,SLP,ASP,Reca0,minSLP): #
    # calculating the dH/tan(alpha)=f(aspect) and fitting a sinusoid in it
    maskNAN=np.where(np.isnan(dH*ASP*SLP)==False)
    dH=dH[maskNAN]
    SLP=SLP[maskNAN]
    ASP=ASP[maskNAN]
    xxdata = ASP.flatten()
    yydata = dH.flatten()/np.tan(np.deg2rad(SLP.flatten()))
    yydata[SLP.flatten()<minSLP] = np.float32(np.nan)
    fnan = ~np.isnan(yydata)
    xxdata = xxdata[fnan]
    yydata = yydata[fnan]
    
    # first calculate the median for bined aspect before fiting
    xdata = []
    ydata = []
    step_asp = 20.
    for asp in np.arange(0,360,step_asp):
        xdata.append(asp+step_asp/2)
        ydata.append(np.median(yydata[(xxdata>asp) & (xxdata<asp+step_asp)]))
    xdata = np.array(xdata)
    ydata = np.array(ydata)
            
    # fit
    fit, pcov = optimization.curve_fit(f, xdata[~np.isnan(ydata)], ydata[~np.isnan(ydata)] ,Reca0,method = 'trf') #[0] #method : {‘lm’, ‘trf’, ‘dogbox’},
    
    plt.figure(1)
    plt.plot(np.arange(0.5,360.5,step_asp),ydata-f(np.arange(0.5,360.5,step_asp), fit[0], fit[1], fit[2]), 'g')
                        
    # ! Calculate dX,dY ! #
    dX = np.round(fit[0]*np.sin(np.deg2rad(fit[1])),4)
    dY = np.round(fit[0]*np.cos(np.deg2rad(fit[1])),4)
    Reca0 = np.array([fit[0],fit[1], np.median(ydata[~np.isnan(ydata)])])
    return dX,dY,Reca0

def myget_dH(DEMm,DEMr,nNMAD): 
    dH = DEMm-DEMr
    if nNMAD is not None:
        dH[abs(dH-np.nanmedian(dH))-nNMAD*nmad(dH.flatten())>0] = np.float32(np.nan)
        
    return dH


def mygetStat_dH(dH,Reca_Stat): 
    Reca_Stat['MEAN_resi'].append(np.round(np.nanmean(dH.flatten()),3))
    Reca_Stat['MEDIAN_resi'].append(np.round(np.nanmedian(dH.flatten()),3))
    Reca_Stat['STD_resi'].append(np.round(np.nanstd(dH.flatten()),3))
    Reca_Stat['NMAD_resi'].append(np.round(nmad(dH.flatten()),3))
    return Reca_Stat

def myTestRECA(NMAD_resi,thresholdNMAD,N,Nmax,thresholdXYvec,dX,dY):
    # Condition on NMAD improvement
    Improvement_NMAD_abs=np.round(NMAD_resi[-1]-NMAD_resi[-2],4)
    Improvement_NMAD_pct=np.round((100*((Improvement_NMAD_abs)/NMAD_resi[-2])),3)

    # Condition on number of iteration
    if N>1:
        testNMAD = ( np.abs(Improvement_NMAD_pct)> thresholdNMAD )
    else:
        testNMAD = True
    testN = ( N < Nmax )
    
    # Condition on vector length
    if (np.sqrt(np.square(dX)+np.square(dY)) > thresholdXYvec) and (dX!=0) and (dY!=0):
        testXYvec=True
    else:
        testXYvec=False
    
    print('dX is :'+str(dX))
    print('dY is :'+str(dY))
    print('testXY is : '+str(testXYvec))
    print('testNMAD is : '+str(testNMAD))
    print('testN is : '+str(testN))
    testRECA = testNMAD*testN*testXYvec
    
    if Improvement_NMAD_pct<0:
        print('This loop improved NMAD by '+str(np.abs(Improvement_NMAD_pct))+' % or '+str(Improvement_NMAD_abs)+' m (previous NMAD :'+str(NMAD_resi[-2])+' m, current NMAD : '+str(NMAD_resi[-1])+' m)')
    else:
        print('WARNING This loop degraded NMAD by '+str(np.abs(Improvement_NMAD_pct))+' % or '+str(Improvement_NMAD_abs)+' m (previous NMAD :'+str(NMAD_resi[-2])+' m, current NMAD : '+str(NMAD_resi[-1])+' m)') 
    return testRECA

def mygetICESATdata(IceSatfile): 
    X=np.genfromtxt(IceSatfile,dtype=float,usecols=0,delimiter=',',skip_header=0)
    Y=np.genfromtxt(IceSatfile,dtype=float,usecols=1,delimiter=',',skip_header=0)
    Z=np.genfromtxt(IceSatfile,dtype=float,usecols=2,delimiter=',',skip_header=0)
    return X,Y,Z

def myinterp2d_point(Xref,Yref,Zref,X,Y,nbuffer): 
    Znew=[]
    for ii in np.arange(0,len(X)):
        try:
            nX=np.where(Xref>X[ii])[0][0]
            nY=np.where(Yref>Y[ii])[0][0]
            if (nX>nbuffer) & (nY>nbuffer) & (nX<len(Xref)-nbuffer) & (nY<len(Yref)-nbuffer):
                Zref_interpolate=scipy.interpolate.interp2d(Xref[nX-nbuffer:nX+nbuffer],Yref[nY-nbuffer:nY+nbuffer],Zref[nY-nbuffer:nY+nbuffer,nX-nbuffer:nX+nbuffer]) # !!! etape longue !!!
                Znew.append(Zref_interpolate(X[ii],Y[ii])[0])
            else:
                Znew.append(np.nan)
        except IndexError as error:
            Znew.append(np.nan)
            
    return np.array(np.squeeze(Znew))


def Coreg_Nuth_Kaab_4icesat( IceSat_file = None, DEMref = None, SLPrst = None, ASPrst = None, thresholdNMAD = 1, thresholdXYvec=0.1, Nmax = 10, minSLP = 4, maxSLP = 45, nNMAD = None, outDir=None):
    print('\n START OF CO-REGISTRATRION \n')
    
    if not os.path.exists(outDir):
        os.mkdir(outDir)
    
    print('Open DEM ref')
    DEMr0,mask_ND=mygetRSTdata(DEMref)
    Xr,Yr=mygetRSTgrid(DEMref)
    DEMr=DEMr0.astype('float32')
    DEMr0=None 
    
    # Calculate slope if not provided
    if SLPrst is None:
        print('Calculating slope. Writting in '+outDir+'/tmp-SLP.tif')
        os.system("gdaldem slope '"+ DEMref+"' "+outDir+"/tmp-SLP.tif")
        SLPrst = outDir+'/tmp-SLP.tif'
    
    # Calculate aspect if not provided
    if ASPrst is None:
        print('Calculating aspect. Writting in '+outDir+'/tmp-ASP.tif')
        os.system("gdaldem aspect '"+ DEMref+"' "+outDir+"/tmp-ASP.tif")
        ASPrst = outDir+'/tmp-ASP.tif'
        
    print('Open slope raster')
    SLP0,mask_tmp=mygetRSTdata(SLPrst)
    # mask too steep slopes
    DEMr[SLP0>maxSLP]=np.nan
    DEMr[SLP0<minSLP]=np.nan
    SLP = SLP0.astype('float32')
    SLP0=None
    
    print('Open aspect raster')
    ASP0,mask_tmp=mygetRSTdata(ASPrst)
    mask_ND = mask_ND*mask_tmp #((ASP0!= Band.GetNoDataValue()))
    mask_tmp=None
    ASP = ASP0.astype('float32')
    ASP0=None

    testRECA = 1
    Reca_Vec={'dX':[],'dY':[]}
    Reca_Stat={'MEAN_resi':[],'MEDIAN_resi':[],'STD_resi':[],'NMAD_resi':[]}
    Reca0 = np.array((1,1,0))

    # Open ICESat data
    print('\n\n\nWorking on file : '+IceSat_file)
    Xm0,Ym0,Zm0=mygetICESATdata(IceSat_file)
    Zm=np.copy(Zm0)
    Xm=np.copy(Xm0)
    Ym=np.copy(Ym0)
    
    # calculate dH
    Zr_point=myinterp2d_point(Xr,Yr,DEMr,Xm,Ym,10)
    
    ASP_point=myinterp2d_point(Xr,Yr,ASP,Xm,Ym,10)
    
    SLP_point=myinterp2d_point(Xr,Yr,SLP,Xm,Ym,10)
    
    dH = myget_dH(Zm,Zr_point,nNMAD)
    dH0=np.copy(dH)
    Reca_Stat=mygetStat_dH(dH,Reca_Stat)

    
    Nloop=0
    while testRECA:
        Nloop+=1        
        print('-----------------------------')
        print('Iteration number '+str(Nloop))
                    
        # calculate dX,dY
        dX,dY,Reca0 = mygetNnK_dXdY(dH,SLP_point,ASP_point,Reca0,minSLP)
        Reca_Vec['dX'].append(dX)
        Reca_Vec['dY'].append(dY)
        
        # apply dX,dY to DEMm
        Xm=Xm0-np.sum(Reca_Vec['dX'])
        Ym=Ym0-np.sum(Reca_Vec['dY'])
        
        Zr_point=myinterp2d_point(Xr,Yr,DEMr,Xm,Ym,10)
        ASP_point=myinterp2d_point(Xr,Yr,ASP,Xm,Ym,10)
        SLP_point=myinterp2d_point(Xr,Yr,SLP,Xm,Ym,10)
        dH = myget_dH(Zm,Zr_point,nNMAD)
        
        #mymakeFig_dH(dH,Xr,Yr,outDir,'initial')    
        Reca_Stat=mygetStat_dH(dH,Reca_Stat)  
        
        testRECA=myTestRECA(Reca_Stat['NMAD_resi'],thresholdNMAD,Nloop,Nmax,thresholdXYvec,dX,dY)
    
    if Reca_Stat['NMAD_resi'][-1]-Reca_Stat['NMAD_resi'][-2]>0: # degradation of NMAD
        Reca_Vec['dX'][-1]=0
        Reca_Vec['dY'][-1]=0
        
    # write co-registered data
    X2write = Xm0-np.sum(Reca_Vec['dX'])
    Y2write = Ym0-np.sum(Reca_Vec['dY'])   
    dZmean2write = np.nanmean(dH) 
    dZmedian2write = np.nanmedian(dH)
    dZmode2write = mymode(dH, 30, 0.05)
 
    outName=os.path.basename(IceSat_file).split('.csv')[0]
    with open(outDir+'/'+outName+'_coreg.csv', 'w') as fout:
        np.savetxt(fout,np.hstack((np.expand_dims(X2write,1),np.expand_dims(Y2write,1),np.expand_dims(Zm,1))),fmt='%.2f, %.2f, %.2f',header='X,Y,Z',delimiter=',')
        
    # figure showing histogram before and after coreg
    plt.figure()
    plt.hist(dH0.flatten(),bins=50,range=(-10,10),histtype='step',color='k',label='before')   
    plt.hist(dH.flatten(),bins=50,range=(-10,10),histtype='step',color='g',label='after')   
    plt.legend()
    plt.grid(True)
    plt.savefig(outDir+'/hist_dH_'+outName+'.png')
    plt.close()
    
    # writing report
    with open(outDir+'/log_reca_NnK_'+outName+'.csv', 'w') as csvfile:
        writer  = csv.writer(csvfile, delimiter = ',')
        writer.writerow(['dX', 'dY', 'NMAD dH init', 'NMAD dH final', 'Mean dH', 'Median dH', "Mode dH"])
        writer.writerow([str(np.sum(Reca_Vec['dX']).round(2)), str(np.sum(Reca_Vec['dY']).round(2)),str(Reca_Stat['NMAD_resi'][0]),str(Reca_Stat['NMAD_resi'][-1]),str(np.round(dZmean2write,2)),str(np.round(dZmedian2write,2)),str(np.round(dZmode2write,2))]) 
        
    print('\n END CO-REGISTRATION')

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description = "Co-register IceSat tracks to a reference DEM. Mob(ile) moves to fit Ref(erence).")
    parser.add_argument("-IceSat_file",dest = "IceSat_file")
    parser.add_argument("-DEMref",dest = "DEMref")
    parser.add_argument("-SLPrst",dest = "SLPrst")
    parser.add_argument("-ASPrst",dest = "ASPrst")
    parser.add_argument("-outDir",dest="outDir")
    parser.add_argument("-thresholdNMAD",dest = "thresholdNMAD",type = float)
    parser.add_argument("-thresholdXYvec",dest = "thresholdXYvec",type = float)
    parser.add_argument("-Nmax",dest = "Nmax",type = int)
    parser.add_argument("-minSLP",dest = "minSLP",type = float)
    parser.add_argument("-maxSLP",dest = "maxSLP",type = float)
    parser.add_argument("-nNMAD",dest = "nNMAD", type = int)
    args = parser.parse_args()

    Coreg_Nuth_Kaab_4icesat( args.IceSat_file, args.DEMref, args.SLPrst, args.ASPrst, args.thresholdNMAD, args.thresholdXYvec, args.Nmax, args.minSLP, args.maxSLP, args.nNMAD, args.outDir)
